package com.bronzelabs.poiissue;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

@SuppressWarnings("serial")
public class JakartaPOIIssueServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		// Set headers
		resp.setContentType("application/zip");
		resp.setHeader("Content-Disposition", String.format("attachment;filename=%s", "file.xlsx"));
		resp.setHeader("Cache-Control", "no-cache"); 											// For HTTP 1.1
		resp.setHeader("Pragma", "no-cache"); 													// For HTTP 1.0
		resp.setDateHeader("Expires", 0); 	
		
		// Create workbook
		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheet = wb.createSheet("new sheet");

		XSSFRow row = sheet.createRow((short) 0);
		row.createCell(0).setCellValue("hello");

		wb.write(resp.getOutputStream());

		wb.close();
	}
}
