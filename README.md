# Jakarta POI App Engine Issue

**Note: this issue only occurs on the development server.**

If you attempt to create an XLSX file (i.e. the newer Excel format) on App Engine (1.9.18) you get the following error:


```
Mar 06, 2015 11:07:08 AM com.google.apphosting.utils.jetty.JettyLogger warn
WARNING: /jakartapoiissue
org.apache.poi.POIXMLException: org.apache.poi.POIXMLException: java.lang.IllegalAccessException: Class com.google.appengine.tools.development.agent.runtime.Runtime can not access a member of class org.apache.poi.xssf.usermodel.XSSFSheet with modifiers "protected"
	at org.apache.poi.POIXMLDocumentPart.createRelationship(POIXMLDocumentPart.java:394)
	at org.apache.poi.POIXMLDocumentPart.createRelationship(POIXMLDocumentPart.java:358)
	at org.apache.poi.xssf.usermodel.XSSFWorkbook.createSheet(XSSFWorkbook.java:758)
	at com.bronzelabs.poiissue.JakartaPOIIssueServlet.doGet(JakartaPOIIssueServlet.java:26)
	at javax.servlet.http.HttpServlet.service(HttpServlet.java:617)
	at javax.servlet.http.HttpServlet.service(HttpServlet.java:717)
	at org.mortbay.jetty.servlet.ServletHolder.handle(ServletHolder.java:511)
	at org.mortbay.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1166)
	at com.google.appengine.api.socket.dev.DevSocketFilter.doFilter(DevSocketFilter.java:74)
	at org.mortbay.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1157)
	at com.google.appengine.tools.development.ResponseRewriterFilter.doFilter(ResponseRewriterFilter.java:127)
	at org.mortbay.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1157)
	at com.google.appengine.tools.development.HeaderVerificationFilter.doFilter(HeaderVerificationFilter.java:34)
	at org.mortbay.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1157)
	at com.google.appengine.api.blobstore.dev.ServeBlobFilter.doFilter(ServeBlobFilter.java:63)
	at org.mortbay.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1157)
	at com.google.apphosting.utils.servlet.TransactionCleanupFilter.doFilter(TransactionCleanupFilter.java:43)
	at org.mortbay.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1157)
	at com.google.appengine.tools.development.StaticFileFilter.doFilter(StaticFileFilter.java:125)
	at org.mortbay.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1157)
	at com.google.appengine.tools.development.DevAppServerModulesFilter.doDirectRequest(DevAppServerModulesFilter.java:366)
	at com.google.appengine.tools.development.DevAppServerModulesFilter.doDirectModuleRequest(DevAppServerModulesFilter.java:349)
	at com.google.appengine.tools.development.DevAppServerModulesFilter.doFilter(DevAppServerModulesFilter.java:116)
	at org.mortbay.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1157)
	at org.mortbay.jetty.servlet.ServletHandler.handle(ServletHandler.java:388)
	at org.mortbay.jetty.security.SecurityHandler.handle(SecurityHandler.java:216)
	at org.mortbay.jetty.servlet.SessionHandler.handle(SessionHandler.java:182)
	at org.mortbay.jetty.handler.ContextHandler.handle(ContextHandler.java:765)
	at org.mortbay.jetty.webapp.WebAppContext.handle(WebAppContext.java:418)
	at com.google.appengine.tools.development.DevAppEngineWebAppContext.handle(DevAppEngineWebAppContext.java:98)
	at org.mortbay.jetty.handler.HandlerWrapper.handle(HandlerWrapper.java:152)
	at com.google.appengine.tools.development.JettyContainerService$ApiProxyHandler.handle(JettyContainerService.java:491)
	at org.mortbay.jetty.handler.HandlerWrapper.handle(HandlerWrapper.java:152)
	at org.mortbay.jetty.Server.handle(Server.java:326)
	at org.mortbay.jetty.HttpConnection.handleRequest(HttpConnection.java:542)
	at org.mortbay.jetty.HttpConnection$RequestHandler.headerComplete(HttpConnection.java:923)
	at org.mortbay.jetty.HttpParser.parseNext(HttpParser.java:547)
	at org.mortbay.jetty.HttpParser.parseAvailable(HttpParser.java:212)
	at org.mortbay.jetty.HttpConnection.handle(HttpConnection.java:404)
	at org.mortbay.io.nio.SelectChannelEndPoint.run(SelectChannelEndPoint.java:409)
	at org.mortbay.thread.QueuedThreadPool$PoolThread.run(QueuedThreadPool.java:582)
Caused by: org.apache.poi.POIXMLException: java.lang.IllegalAccessException: Class com.google.appengine.tools.development.agent.runtime.Runtime can not access a member of class org.apache.poi.xssf.usermodel.XSSFSheet with modifiers "protected"
	at org.apache.poi.xssf.usermodel.XSSFFactory.newDocumentPart(XSSFFactory.java:73)
	at org.apache.poi.POIXMLDocumentPart.createRelationship(POIXMLDocumentPart.java:379)
	... 40 more
Caused by: java.lang.IllegalAccessException: Class com.google.appengine.tools.development.agent.runtime.Runtime can not access a member of class org.apache.poi.xssf.usermodel.XSSFSheet with modifiers "protected"
	at sun.reflect.Reflection.ensureMemberAccess(Reflection.java:105)
	at com.google.appengine.tools.development.agent.runtime.Runtime$22.run(Runtime.java:503)
	at java.security.AccessController.doPrivileged(Native Method)
	at com.google.appengine.tools.development.agent.runtime.Runtime.checkAccess(Runtime.java:500)
	at com.google.appengine.tools.development.agent.runtime.Runtime.checkAccess(Runtime.java:494)
	at com.google.appengine.tools.development.agent.runtime.Runtime.newInstance_(Runtime.java:138)
	at com.google.appengine.tools.development.agent.runtime.Runtime.newInstance(Runtime.java:150)
	at org.apache.poi.xssf.usermodel.XSSFFactory.newDocumentPart(XSSFFactory.java:71)
	... 41 more

```

Issue logged with Apache but they responded by saying that it's an issue with the class loader used by App Engine: [https://bz.apache.org/bugzilla/show_bug.cgi?id=55665](https://bz.apache.org/bugzilla/show_bug.cgi?id=55665)

An example of this running live (and working) can be found here: [http://bronze-gae-poi-issue.appspot.com](http://bronze-gae-poi-issue.appspot.com)
